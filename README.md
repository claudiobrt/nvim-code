# nvim-code

nvim-code is a fork of [Nvim Mach 2](https://github.com/ChristianChiarulli/nvim) ,
I want to strip some things out of it and make it work for me. It is a great starting point, so thank you for your work [ChristianChiarulli](https://github.com/ChristianChiarulli/) .

## Install in one command

The following will install this config if you have an existing config it will move it to `~/.config/nvim.old`

This script only supports Mac , Ubuntu and Arch

```
bash <(curl -s https://gitlab.com/claudiobrt/nvim-code/-/raw/master/utils/install.sh)

```

## Install Neovim

- On Mac

  ```
  brew install neovim
  ```

- Ubuntu

  ```
  sudo apt install neovim
  ```

- Arch

  ```
  sudo pacman -S neovim
  
  ```

## Clone this repo into your config

```

git clone https://gitlab.com/claudiobrt/nvim.git ~/.config/nvim

```

## Install python & node support

```

pip install pynvim

```

```

npm i -g neovim

```

## Install Neovim remote

```

pip install neovim-remote

```

This will install `nvr` to `~/.local/bin` so you will need to add the following to your `bashrc` or `zshrc`

```

export PATH=$HOME/.local/bin:$PATH

```

## Install clipboard support

- On Ubuntu

```

sudo apt install xsel

```

- On Arch Linux

```

sudo pacman -S xsel

```

## List of programs you should install

- ranger
- ueberzug
- ripgrep
- silver_searcher
- fd
- universal-ctags
- lazy git

Since CoC doesn't support all languages in there extensions
I recommend installing some language servers from scratch
and adding them to your `coc-settings.json` file

Example:

- bash

`npm i -g bash-language-server`

```

"languageserver": {
"bash": {
"command": "bash-language-server",
"args": ["start"],
"filetypes": ["sh"],
"ignoredRootPaths": ["~"]
}
}

```

To use tabnine enter the following in a buffer:

```

TabNine::config

```

## Vim Gists

To use **vim-gists** you will need to configure the following:

```

git config --global github.user <username>

```

